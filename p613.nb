(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      3949,        126]
NotebookOptionsPosition[      3224,        105]
NotebookOutlinePosition[      3562,        120]
CellTagsIndexPosition[      3519,        117]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"$Assumptions", "=", 
  RowBox[{
   RowBox[{"x", ">", "0"}], "&&", 
   RowBox[{"x", "<", "3"}], "&&", 
   RowBox[{"y", ">", "0"}], "&&", 
   RowBox[{"y", "<", "4"}]}]}]], "Input",
 CellChangeTimes->{{3.732353465912895*^9, 
  3.732353505571632*^9}},ExpressionUUID->"294bafc2-37c6-46f2-8437-\
1cc4468d5087"],

Cell[BoxData[
 RowBox[{
  RowBox[{"x", ">", "0"}], "&&", 
  RowBox[{"x", "<", "3"}], "&&", 
  RowBox[{"y", ">", "0"}], "&&", 
  RowBox[{"y", "<", "4"}]}]], "Output",
 CellChangeTimes->{
  3.732353506030054*^9},ExpressionUUID->"1b6358f2-bf77-451d-b0ff-\
6d458b10af11"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"p", "[", 
   RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
  RowBox[{"1", "-", 
   FractionBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"ArcTan", "[", 
       FractionBox["x", "y"], "]"}], "+", 
      RowBox[{"ArcTan", "[", 
       FractionBox[
        RowBox[{"3", "-", "x"}], "y"], "]"}], "+", 
      RowBox[{"ArcTan", "[", 
       FractionBox["y", "x"], "]"}], "+", 
      RowBox[{"ArcTan", "[", 
       FractionBox[
        RowBox[{"4", "-", "y"}], "x"], "]"}]}], ")"}], 
    RowBox[{"2", "*", "Pi"}]]}]}]], "Input",
 CellChangeTimes->{{3.732352671814559*^9, 3.7323527284201527`*^9}, {
  3.732353205941482*^9, 
  3.7323532152909527`*^9}},ExpressionUUID->"12690fb5-dfd3-4af9-b8b8-\
d7f2e0a03649"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "[", 
  RowBox[{
   FractionBox[
    RowBox[{"Integrate", "[", 
     RowBox[{
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{"p", "[", 
         RowBox[{"x", ",", "y"}], "]"}], ",", 
        RowBox[{"{", 
         RowBox[{"y", ",", "0", ",", 
          RowBox[{"4", "-", 
           RowBox[{
            FractionBox["4", "3"], "*", "x"}]}]}], "}"}]}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "3"}], "}"}]}], "]"}], 
    RowBox[{
     FractionBox["1", "2"], "*", "3", "*", "4"}]], ",", "10"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.732352731828453*^9, 3.732352776502351*^9}, {
  3.7323528558726463`*^9, 3.732352860729253*^9}, {3.7323533851561937`*^9, 
  3.732353445109991*^9}, {3.732353755246543*^9, 
  3.732353851890429*^9}},ExpressionUUID->"f0a12846-88be-4de2-8352-\
efa04dcb07c5"],

Cell[BoxData["0.39167215040874938765642636944824310788`10."], "Output",
 CellChangeTimes->{
  3.7323528412512197`*^9, 3.732352942972793*^9, 3.732353116715549*^9, 
   3.732353326134404*^9, {3.732353415129881*^9, 3.73235343317656*^9}, {
   3.732353762831822*^9, 3.732353811086248*^9}, {3.732353843536338*^9, 
   3.7323538621668777`*^9}},ExpressionUUID->"fbfa2260-a516-412f-b917-\
26a5d870b21a"]
}, Open  ]]
},
WindowSize->{1364, 726},
WindowMargins->{{1, Automatic}, {1, Automatic}},
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 328, 9, 31, "Input",ExpressionUUID->"294bafc2-37c6-46f2-8437-1cc4468d5087"],
Cell[911, 33, 267, 8, 35, "Output",ExpressionUUID->"1b6358f2-bf77-451d-b0ff-6d458b10af11"]
}, Open  ]],
Cell[1193, 44, 735, 22, 63, "Input",ExpressionUUID->"12690fb5-dfd3-4af9-b8b8-d7f2e0a03649"],
Cell[CellGroupData[{
Cell[1953, 70, 860, 24, 72, "Input",ExpressionUUID->"f0a12846-88be-4de2-8352-efa04dcb07c5"],
Cell[2816, 96, 392, 6, 35, "Output",ExpressionUUID->"fbfa2260-a516-412f-b917-26a5d870b21a"]
}, Open  ]]
}
]
*)

